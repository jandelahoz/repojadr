var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var port = process.env.PORT || 3000;
var URI_PATH = '/colapi/v1';
var USER_FILE = './users.data.json';
var USER_FILE_DATA = require( USER_FILE );

app.listen(port, function(){
  console.log("API apicol escuchando en el puerto " + port + "...");
});

app.use(bodyParser.json()); // for parsing application/json

app.get(URI_PATH+"/users", function (req, res) {
  res.sendfile( USER_FILE );
});

app.get(URI_PATH+"/users/:id", function (req, res) {
  var id = req.params.id - 1;
  res.send( USER_FILE_DATA[id] || {"msg":"No existe registro !!!"} );
});

app.post(URI_PATH + '/users',
 function(req, res) {
   var newID = USER_FILE_DATA.length + 1;
   var newUser = {
     "id" : newID,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "country" : req.body.country
   };
   USER_FILE_DATA.push(newUser);
   console.log(USER_FILE_DATA);
   res.send({"msg" : "Usuario creado correctamente: ", newUser});
 });

app.delete(URI_PATH + "/users/:id", function (req, res) {
  var idSearch = req.params.id;
  if ( USER_FILE_DATA[idSearch-1] != undefined ) {
    USER_FILE_DATA.splice( idSearch-1, 1 )
    res.send({"msg" : "Usuario eliminado correctamente." });
  }
  res.send({"msg" : "Usuario no encontrado."});
});

app.put(URI_PATH + "/users/:id", function (req, res) {
  var idSearch = req.params.id;
  if ( USER_FILE_DATA[idSearch-1] != undefined ) {
    USER_FILE_DATA[idSearch-1] = req.body;
    res.send({"msg" : "Usuario actualizado correctamente." + req.body });
  }
  res.send({"msg" : "Usuario no encontrado."});
});
