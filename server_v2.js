var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var port = process.env.PORT || 3001;
var URI_PATH = '/colapi/v2';
var USER_FILE = './users.data.pwd.json';
var USER_FILE_DATA = require( USER_FILE );

app.listen(port, function(){
  console.log("API apicol escuchando en el puerto " + port + "...");
});

app.use(bodyParser.json()); // for parsing application/json

app.post(URI_PATH + '/logIn',function(req, res) {

   if ( req.body.email === undefined || req.body.password === undefined ){
     res.send( 409, {"msg":"Wrong Parameters !!!"} );
   } else {
     var email = req.body.email;
     var password = req.body.password;
     var foundUser = false;

    for ( listUsers of USER_FILE_DATA) {
      if ( listUsers.email == email && listUsers.password == password ){
          if ( listUsers.session === undefined || listUsers.session !== true ){
            listUsers.session = true;
            writeInFileUser( USER_FILE_DATA );
            console.log( "LogIn: "+listUsers.id );
          }
          foundUser = true;
          res.send( {"msg":"User loggin !!!"} );
      }
    }

    if ( !foundUser ){
      res.send( 403, {"msg":"Email or Password invalid !!!"} );
    }

   }

 });

app.post( URI_PATH + '/logOut', function(req, res){
  if ( req.body.email === undefined ){
    res.send( 409, {"msg":"Wrong Parameters !!!"} );
  } else {
    var email = req.body.email;
    var foundUser = false;

   for ( listUsers of USER_FILE_DATA) {
     if ( listUsers.email == email  ){
        listUsers.session = undefined;
        writeInFileUser( USER_FILE_DATA );
        console.log( "LogOut: "+listUsers.email );
        foundUser = true;
        res.send( {"msg":"User logout !!!"} );
     }
   }

   if ( !foundUser ){
     res.send( 404, {"msg":"User not found !!!"} );
   }

  }
});

function writeInFileUser( data ){
 var fs = require('fs');
 fs.writeFile( USER_FILE, JSON.stringify(data), 'utf8', function(err){
   console.log( (err)?err:"Datos guardados exitosamente en: "+USER_FILE );
 });
}
