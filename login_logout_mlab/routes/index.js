'use strict'

const express=require('express');
const authController=require('../controllers/auth');
const api = express.Router();

/*
  Login - Logout
*/
api.post('/login',authController.login);
api.post('/logout',authController.logout);

module.exports=api;
