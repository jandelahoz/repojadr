module.exports={
  port:process.env.PORT|| 3000,
  mlab_host:process.env.MLAB_HOST||'https://api.mlab.com/api/1/databases/',
  mlab_db:process.env.MLAB_DB||'dbmongo_test/',
  mlab_key:process.env.MLAB_KEY||'apiKey=ZUFaPdqIFuB7cR2UcLyInZo3H5Tvekuk',
  collection_user:process.env.COLLECTION_USER||'user',
  URLbase:process.env.URL_BASE||'/colapi/v3/'
}
