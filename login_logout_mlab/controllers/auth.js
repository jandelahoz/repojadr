'use strict'

var requestJson = require('request-json');
const config = require('../config');
const url = config.mlab_host + config.mlab_db + 'collections/';

/*
  login user
*/
function login(req, res){
  if ( req.body.email === undefined || req.body.password === undefined ){
    res.status(409).send({"msg":"Wrong Parameters !!!"} );
  } else {
    var email = req.body.email;
    var password = req.body.password;
    var client = requestJson.createClient( url );
    const queryName = 'f={"id": 1, "session": 1}&q={"email":"'+ email +'", "password": "'+ password +'"}&';

    client.get( config.collection_user + '?' + queryName + config.mlab_key, function(errOne, resOne, bodyOne) {
      var respQuery=bodyOne[0];
      if ( respQuery != undefined ){
        if ( respQuery.session != true ) {
          var data = { "session" : true };
          var change = '{"$set":' + JSON.stringify(data) + '}';

          client.put(config.collection_user+'?q={"id": "' + respQuery.id + '"}&' + config.mlab_key, JSON.parse(change), function(errSec, resSec, bodySec) {
            console.log("User login:" + respQuery.id);
            res.status(200).send({"msg":"Welcome "+respQuery.id+" !!!"});
          });
        } else {
          console.log("User login active:" + respQuery.id);
          res.status(200).send({"msg":"Session actived "+respQuery.id+" !!!"});
        }
      } else {
        res.status(401).send({"msg":"Email or Password invalid !!!"} );
      }

    });
  }

}

/*
  logout user
*/
function logout(req, res){
    if ( req.body.email === undefined ){
      res.status(409).send({"msg":"Wrong Parameters !!!"} );
    } else {
      var email = req.body.email;
      var client = requestJson.createClient( url );
      const queryName = 'f={"id": 1}&q={"email":"'+ email +'"}&';

      client.get(config.collection_user+'?'+queryName+config.mlab_key, function(errOne, resOne, bodyOne) {
          var respQuery = bodyOne[0];
          if ( respQuery != undefined ) {
            var data = { "session" : false };
            var change = '{"$unset":' + JSON.stringify(data) + '}';

            client.put(config.collection_user+'?q={"id": "' + respQuery.id + '"}&' + config.mlab_key, JSON.parse(change), function(errSec, resSec, bodySec) {
              console.log("User logout:" + respQuery.id);
              res.status(200).send({"msg":"Comming soon "+respQuery.id+" !!!"});
            });
          } else {
            res.status(401).send({"msg":"Email invalid !!!"});
          }
      });

    }
}

module.exports={
  login,
  logout
};
